-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2019 at 08:53 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `schoolmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrations`
--

CREATE TABLE `administrations` (
  `AdministrationId` int(10) UNSIGNED NOT NULL,
  `LoginId` int(10) UNSIGNED NOT NULL,
  `FullName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Qualification` text COLLATE utf8mb4_unicode_ci,
  `MaritalStatus` enum('Married','Unmarried','Engaged') COLLATE utf8mb4_unicode_ci DEFAULT 'Unmarried',
  `Contact` int(10) UNSIGNED NOT NULL,
  `TemporaryAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PermanentAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Gender` enum('Male','Female','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Salary` int(10) UNSIGNED DEFAULT NULL,
  `DateEnrolled` datetime DEFAULT CURRENT_TIMESTAMP,
  `AdministrationType` enum('Accountant','Principal','Vice_Principal','Incharge') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpadeted` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `ArticleId` int(10) UNSIGNED NOT NULL,
  `StudentsId` int(10) UNSIGNED NOT NULL,
  `Title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateSubbmitted` datetime DEFAULT CURRENT_TIMESTAMP,
  `ApproveStatus` enum('Approved','Rejected','Pending') COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `ApproverId` int(10) UNSIGNED DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attendence`
--

CREATE TABLE `attendence` (
  `AttendenceId` bigint(20) UNSIGNED NOT NULL,
  `ClassroomId` int(10) UNSIGNED DEFAULT NULL,
  `StudentsId` int(10) UNSIGNED DEFAULT NULL,
  `PresentStatus` enum('Present','Absent') COLLATE utf8mb4_unicode_ci DEFAULT 'Present',
  `PresentDate` datetime DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill`
--

CREATE TABLE `bill` (
  `FeesId` int(10) UNSIGNED NOT NULL,
  `FeesType` enum('Admission','Tution','Exam','Lab','Transport','Hostel','Library','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `InvoiceNo` int(10) UNSIGNED NOT NULL,
  `AmountPaid` enum('Paid','Not Paid') COLLATE utf8mb4_unicode_ci DEFAULT 'Not Paid',
  `DueType` enum('Dr. Due','Cr. Due') COLLATE utf8mb4_unicode_ci NOT NULL,
  `DueAmount` int(10) UNSIGNED NOT NULL,
  `Amount` int(10) UNSIGNED NOT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE `classroom` (
  `ClassroomId` int(10) UNSIGNED NOT NULL,
  `ClassName` enum('One','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Section` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ClassTeacher` int(10) UNSIGNED DEFAULT NULL,
  `ClassRepresentativeBoy` int(11) DEFAULT NULL,
  `ClassRepresentativeGirl` int(11) DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `DepartmentId` int(10) UNSIGNED NOT NULL,
  `DepartmentName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Subjects` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `TeachersId` int(10) UNSIGNED DEFAULT NULL,
  `DateInserted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feestructure`
--

CREATE TABLE `feestructure` (
  `ClassFeesId` int(10) UNSIGNED NOT NULL,
  `ClassroomId` int(10) UNSIGNED NOT NULL,
  `AdmissionFees` int(10) UNSIGNED DEFAULT NULL,
  `TutionFees` int(10) UNSIGNED DEFAULT NULL,
  `ExamFees` int(10) UNSIGNED DEFAULT NULL,
  `ComputerLabFees` int(10) UNSIGNED DEFAULT NULL,
  `ScienceLabFees` int(10) UNSIGNED DEFAULT NULL,
  `LibraryFees` int(10) UNSIGNED DEFAULT NULL,
  `Hostelfees` int(10) UNSIGNED DEFAULT NULL,
  `TransportFees` int(10) UNSIGNED DEFAULT NULL,
  `DressExpenses` int(10) UNSIGNED DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hostels`
--

CREATE TABLE `hostels` (
  `HostelId` int(10) UNSIGNED NOT NULL,
  `EnrolledDate` date NOT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `Login_Id` int(10) UNSIGNED NOT NULL,
  `UserType` enum('Incharge','Principal','Teacher','VicePrincipal','Student','Parents','Accountant') COLLATE utf8mb4_unicode_ci DEFAULT 'Student',
  `Token` text COLLATE utf8mb4_unicode_ci,
  `RecoveryToken` text COLLATE utf8mb4_unicode_ci,
  `Status` enum('active','inactive') COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `UserName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Passwd` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Image` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `LastLogin` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `LastIP` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateInserted` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`Login_Id`, `UserType`, `Token`, `RecoveryToken`, `Status`, `UserName`, `Passwd`, `Image`, `LastLogin`, `LastIP`, `DateInserted`, `DateUpdated`) VALUES
(1, 'Incharge', '1j65ByFo5Lu9Rn7I23rVvsHRmaJUsT2eyCkgoNd4Ru5UWY34V2LsJm1TkJlXRbT9UEFIJOP5Rkx5dOQL11deOdIxiBm4AYRDag5g', NULL, 'active', 'Incharge@incharge', '1f6d1d9095af973eedfd82b07787b9c632570f99', NULL, '2019-07-12', '127.0.0.1', '2019-07-11 11:14:54', '2019-07-13 00:30:28'),
(2, 'Principal', NULL, NULL, 'active', 'principal@principal', '382e35f95553053e197e4f683a0cede950b73836', NULL, '0000-00-00', '', '2019-07-11 11:16:36', '0000-00-00 00:00:00'),
(3, 'Parents', NULL, NULL, 'active', 'parenst@parents', 'f1ae1991a5b4120f727bef643367831d35be4b10', NULL, '0000-00-00', '', '2019-07-11 11:17:28', '0000-00-00 00:00:00'),
(4, 'Student', NULL, NULL, 'active', 'student@student', '5adf19be992d759fbf10d0d87f659d12856f0a40', NULL, '0000-00-00', '', '2019-07-11 11:22:49', '0000-00-00 00:00:00'),
(5, 'Teacher', NULL, NULL, 'active', 'teacher@teacher', 'ffb82f6830723d8e42aab3336d894699926cf93a', NULL, '0000-00-00', '', '2019-07-11 11:23:32', '0000-00-00 00:00:00'),
(6, 'VicePrincipal', NULL, NULL, 'active', 'viceprincipal@viceprincipal', '848267df795a4013444b0c8c22f4780dc3821dc4', NULL, '0000-00-00', '', '2019-07-11 11:25:03', '0000-00-00 00:00:00'),
(7, 'Accountant', NULL, NULL, 'active', 'accountant@accountant', '8164bbfabaae0f96bc211610e210650d89f533fb', NULL, '0000-00-00', '', '2019-07-11 11:26:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `NoticeId` int(10) UNSIGNED NOT NULL,
  `Title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `SendTo` enum('Parents','Students','Teachers','All') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `IssuedDate` date DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `ParentsId` int(10) UNSIGNED NOT NULL,
  `LoginId` int(10) UNSIGNED NOT NULL,
  `FullName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Contact` int(10) UNSIGNED NOT NULL,
  `TemporaryAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PermanentAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpadeted` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `ResultId` bigint(20) UNSIGNED NOT NULL,
  `ClassroomId` int(10) UNSIGNED NOT NULL,
  `StudentsId` int(10) UNSIGNED NOT NULL,
  `SemesterName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SubjectName` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MarksObtaned` float DEFAULT NULL,
  `PassedStatus` enum('Passed','Failed') COLLATE utf8mb4_unicode_ci DEFAULT 'Passed',
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `StaffsId` int(10) UNSIGNED NOT NULL,
  `FullName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `MaritalStatus` enum('Married','Unmarried','Engaged') COLLATE utf8mb4_unicode_ci DEFAULT 'Unmarried',
  `Contact` int(10) UNSIGNED NOT NULL,
  `TemporaryAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `PermanentAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Gender` enum('Male','Female','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Salary` int(10) UNSIGNED DEFAULT NULL,
  `DateEnrolled` datetime DEFAULT CURRENT_TIMESTAMP,
  `StaffType` enum('Driver','Helper','Assistant','Gardener','Lab Boy','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `studentexpenses`
--

CREATE TABLE `studentexpenses` (
  `StudentExpencesId` int(10) UNSIGNED NOT NULL,
  `ClassfeesId` int(10) UNSIGNED NOT NULL,
  `AmountPaid` int(10) UNSIGNED DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `StudentId` int(10) UNSIGNED NOT NULL,
  `ParentsId` int(10) UNSIGNED NOT NULL,
  `LoginId` int(10) UNSIGNED NOT NULL,
  `FeesId` int(10) UNSIGNED NOT NULL,
  `StudentsExpencesId` int(10) UNSIGNED NOT NULL,
  `ClassroomID` int(10) UNSIGNED NOT NULL,
  `ClassFeesId` int(10) UNSIGNED NOT NULL,
  `FullName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Contact` int(10) UNSIGNED DEFAULT NULL,
  `TemporaryAddress` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PermanentAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Gender` enum('Male','Female','Others') COLLATE utf8mb4_unicode_ci NOT NULL,
  `Batch` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Discount` int(11) DEFAULT NULL,
  `Transport` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT 'No',
  `TransportId` int(10) UNSIGNED DEFAULT NULL,
  `Hostel` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT 'No',
  `HostelId` int(10) UNSIGNED DEFAULT NULL,
  `Image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpadeted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `TeachersId` int(10) UNSIGNED NOT NULL,
  `Login_id` int(10) UNSIGNED DEFAULT NULL,
  `Full_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Email` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Qualification` text COLLATE utf8mb4_unicode_ci,
  `MaritalStatus` enum('Married','Unmarried','Engaged') COLLATE utf8mb4_unicode_ci DEFAULT 'Unmarried',
  `Contact` int(10) UNSIGNED DEFAULT NULL,
  `TemporaryAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Permanent_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Gender` enum('Male','Female','Others') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Salary` int(10) UNSIGNED DEFAULT NULL,
  `DateEnrolled` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Subjects` text COLLATE utf8mb4_unicode_ci,
  `Syllabus` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpadeted` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transports`
--

CREATE TABLE `transports` (
  `TransportId` int(10) UNSIGNED NOT NULL,
  `TransportType` enum('One way','Two way') COLLATE utf8mb4_unicode_ci DEFAULT 'Two way',
  `VehiclesId` int(10) UNSIGNED DEFAULT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `VehicleId` int(10) UNSIGNED NOT NULL,
  `StaffsId` int(10) UNSIGNED NOT NULL,
  `Route` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `DateInserted` datetime DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrations`
--
ALTER TABLE `administrations`
  ADD PRIMARY KEY (`AdministrationId`),
  ADD KEY `Fk_AdministrationsLogin_Refrence_Login` (`LoginId`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`ArticleId`);

--
-- Indexes for table `attendence`
--
ALTER TABLE `attendence`
  ADD PRIMARY KEY (`AttendenceId`);

--
-- Indexes for table `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`FeesId`);

--
-- Indexes for table `classroom`
--
ALTER TABLE `classroom`
  ADD PRIMARY KEY (`ClassroomId`),
  ADD KEY `Fk_Classroom_Refrence_Teacher` (`ClassTeacher`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`DepartmentId`),
  ADD KEY `Fk_Departments_Refrence_Teachers` (`TeachersId`);

--
-- Indexes for table `feestructure`
--
ALTER TABLE `feestructure`
  ADD PRIMARY KEY (`ClassFeesId`),
  ADD KEY `Fk_Feestructure_Refrence_Classroom` (`ClassroomId`);

--
-- Indexes for table `hostels`
--
ALTER TABLE `hostels`
  ADD PRIMARY KEY (`HostelId`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`Login_Id`),
  ADD UNIQUE KEY `user_name` (`UserName`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`NoticeId`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`ParentsId`),
  ADD UNIQUE KEY `Contact` (`Contact`),
  ADD KEY `Fk_ParentsLogin_Refrence_Login` (`LoginId`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`ResultId`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`StaffsId`);

--
-- Indexes for table `studentexpenses`
--
ALTER TABLE `studentexpenses`
  ADD PRIMARY KEY (`StudentExpencesId`),
  ADD KEY `Fk_StudentsExpences_Refrence_ClassFees` (`ClassfeesId`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`StudentId`),
  ADD KEY `Fk_StudentsTransport_Refrence_Transport` (`TransportId`),
  ADD KEY `Fk_StudentsHostel_Refrence_Hostel` (`HostelId`),
  ADD KEY `Fk_StudentsLogin_Refrence_Login` (`LoginId`),
  ADD KEY `Fk_Students_Refrence_Parents` (`ParentsId`),
  ADD KEY `Fk_Students_Refrence_Fees` (`FeesId`),
  ADD KEY `Fk_Students_Refrence_StudentsExpences` (`StudentsExpencesId`),
  ADD KEY `Fk_Students_Refrence_ClassFees` (`ClassFeesId`),
  ADD KEY `Fk_Students_Refrence_Classroom` (`ClassroomID`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`TeachersId`),
  ADD KEY `Fk_TeachersLogin_Refrence_Login` (`Login_id`);

--
-- Indexes for table `transports`
--
ALTER TABLE `transports`
  ADD PRIMARY KEY (`TransportId`),
  ADD KEY `Fk_Transport_Reference_Vehicle` (`VehiclesId`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`VehicleId`),
  ADD KEY `Fk_Vehicles_Refrence_Staffs` (`StaffsId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrations`
--
ALTER TABLE `administrations`
  MODIFY `AdministrationId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `ArticleId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attendence`
--
ALTER TABLE `attendence`
  MODIFY `AttendenceId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bill`
--
ALTER TABLE `bill`
  MODIFY `FeesId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `classroom`
--
ALTER TABLE `classroom`
  MODIFY `ClassroomId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `DepartmentId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feestructure`
--
ALTER TABLE `feestructure`
  MODIFY `ClassFeesId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hostels`
--
ALTER TABLE `hostels`
  MODIFY `HostelId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `Login_Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `NoticeId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `ParentsId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `ResultId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staffs`
--
ALTER TABLE `staffs`
  MODIFY `StaffsId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `studentexpenses`
--
ALTER TABLE `studentexpenses`
  MODIFY `StudentExpencesId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `StudentId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `TeachersId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transports`
--
ALTER TABLE `transports`
  MODIFY `TransportId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `VehicleId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `administrations`
--
ALTER TABLE `administrations`
  ADD CONSTRAINT `Fk_AdministrationsLogin_Refrence_Login` FOREIGN KEY (`LoginId`) REFERENCES `login` (`Login_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `classroom`
--
ALTER TABLE `classroom`
  ADD CONSTRAINT `Fk_Classroom_Refrence_Teacher` FOREIGN KEY (`ClassTeacher`) REFERENCES `teachers` (`TeachersId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `Fk_Departments_Refrence_Teachers` FOREIGN KEY (`TeachersId`) REFERENCES `teachers` (`TeachersId`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `feestructure`
--
ALTER TABLE `feestructure`
  ADD CONSTRAINT `Fk_Feestructure_Refrence_Classroom` FOREIGN KEY (`ClassroomId`) REFERENCES `classroom` (`ClassroomId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parents`
--
ALTER TABLE `parents`
  ADD CONSTRAINT `Fk_ParentsLogin_Refrence_Login` FOREIGN KEY (`LoginId`) REFERENCES `login` (`Login_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `studentexpenses`
--
ALTER TABLE `studentexpenses`
  ADD CONSTRAINT `Fk_StudentsExpences_Refrence_ClassFees` FOREIGN KEY (`ClassfeesId`) REFERENCES `feestructure` (`ClassFeesId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `Fk_StudentsHostel_Refrence_Hostel` FOREIGN KEY (`HostelId`) REFERENCES `hostels` (`HostelId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Fk_StudentsLogin_Refrence_Login` FOREIGN KEY (`LoginId`) REFERENCES `login` (`Login_Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Fk_StudentsTransport_Refrence_Transport` FOREIGN KEY (`TransportId`) REFERENCES `transports` (`TransportId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Fk_Students_Refrence_ClassFees` FOREIGN KEY (`ClassFeesId`) REFERENCES `feestructure` (`ClassFeesId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Fk_Students_Refrence_Classroom` FOREIGN KEY (`ClassroomID`) REFERENCES `classroom` (`ClassroomId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Fk_Students_Refrence_Fees` FOREIGN KEY (`FeesId`) REFERENCES `bill` (`FeesId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Fk_Students_Refrence_Parents` FOREIGN KEY (`ParentsId`) REFERENCES `parents` (`ParentsId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Fk_Students_Refrence_StudentsExpences` FOREIGN KEY (`StudentsExpencesId`) REFERENCES `studentexpenses` (`StudentExpencesId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `Fk_TeachersLogin_Refrence_Login` FOREIGN KEY (`Login_id`) REFERENCES `login` (`Login_Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transports`
--
ALTER TABLE `transports`
  ADD CONSTRAINT `Fk_Transport_Reference_Vehicle` FOREIGN KEY (`VehiclesId`) REFERENCES `vehicles` (`VehicleId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD CONSTRAINT `Fk_Vehicles_Refrence_Staffs` FOREIGN KEY (`StaffsId`) REFERENCES `staffs` (`StaffsId`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
