<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/g-config/config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/g-config/functions.php';

spl_autoload_register(function ($class_name) {
    require_once $_SERVER['DOCUMENT_ROOT'] . '/g-class/' . $class_name . '.php';
});
