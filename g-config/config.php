<?php
ob_start();
session_start();

define('SITE_URL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']); /* http://localhost */


define('CMS_URL', SITE_URL . '/cms');/* SchoolManagement/cms */


define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PWD', '');
define('DB_NAME', 'schoolmanagement');

define('ERROR_LOG', $_SERVER['DOCUMENT_ROOT'] . '/g-error/error.log');




/** Admin path */
define('ADMIN_ASSETS', CMS_URL . '/cms-assets'); /* SchoolManagement/cms/cms-assets */
define('ADMIN_CSS', ADMIN_ASSETS . '/css');/* SchoolManagement/cms/cms-assets/css */
define('ADMIN_JS', ADMIN_ASSETS . '/js');/* SchoolManagement/cms/cms-assets/js */
define('ADMIN_FONTS', ADMIN_ASSETS . '/fonts');/* SchoolManagement/cms/cms-assets/fonts */
define('ADMIN_WEBFONTS', ADMIN_ASSETS . '/webfonts');/* SchoolManagement/cms/cms-assets/webfonts */
define('ADMIN_IMG', ADMIN_ASSETS . '/img');/* SchoolManagement/cms/cms-assets/img */
define('ADMIN_CMS_INC', CMS_URL . '/cms-inc');/* SchoolManagement/cms/cms-inc */
define('ADMIN_Incharge', ADMIN_CMS_INC . '/incharge-inc');/* SchoolManagement/cms/cms-inc/incharge-inc */
define('ADMIN_Principal', ADMIN_CMS_INC . '/principal-inc');/* SchoolManagement/cms/cms-inc/principal-inc */
define('G_CONFIG', SITE_URL . '/g-config');/* SchoolManagement/g-config */
define('G_ERROR', SITE_URL . '/g-error');/* SchoolManagement/g-error */
define('G_CLASS', SITE_URL . '/g-class');/* SchoolManagement/g-class */
define('ADMIN_RENDER', CMS_URL . '/cms-render');/* SchoolManagement/cms/cms-render */


/*** Home path */
// define('ASSETS_URL', SITE_URL.'/cms-assets');
// define('CSS_URL', ASSETS_URL.'/cms-assest/css');
// define('JS_URL', ASSETS_URL.'/js');


define('IMAGE_EXTENSIONS', array('jpg', 'jpeg', 'png', 'bmp', 'svg'));
define('UPLOAD_DIR', $_SERVER['DOCUMENT_ROOT'] . '/cms-uploads');/* SchoolManagement/cms-uploads*/

define('UPLOAD_URL', SITE_URL . '/cms-uploads');/* SchoolManagement/cms-uploads*/


define('SITE_TITLE', 'SMS, A complete School Management System');
