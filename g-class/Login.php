<?php
final class Login extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table('login');
    }

    public function getUserByUserName($UserName)
    {
        // SELECT * FROM users WHERE UserName = $UserName
        // SELECT * FROM users WHERE remeber_token = $token
        // SELECT * FROM users WHERE id = $id
        // SELECT * FROM users
        $args = array(
            // 'fields' => 'id, name, status, password, UserName'
            // 'fields' => ['id', 'name', 'status', 'password', 'UserName']
            'where' => array(
                'UserName' => $UserName
            )
        );
        return $this->select($args);/* Run Select Statement*/
    }

    public function updateUser($data, $user_id)
    {
        $args = array(
            'where' => array(
                'LoginId' => $user_id
            )
        );
        $success = $this->update($data, $args);/* Run update Statement*/
        if ($success) {
            return $user_id;
        } else {
            return false;
        }
    }


    public function getUserByToken($token)
    {
        $args = array(
            'where' => array(
                'Token' => $token
            )
        );
        return $this->select($args);
    }


    public function getAllReporter()
    {
        // SELECT * FROM users WHERE role = 'reporter'
        $args = array(
            'where' => array('role' => 'reporter'),
            'order_by' => ' name ASC'
        );
        return $this->select($args);
    }

    public function getUserByLoginID($LoginId)
    {
        // SELECT * FROM Administration WHERE UserName = $UserName
        // SELECT * FROM Administration WHERE remeber_token = $token
        // SELECT * FROM Administration WHERE id = $id
        // SELECT * FROM Administration
        $args = array(
            // 'fields' => 'id, name, status, password, UserName'
            // 'fields' => ['id', 'name', 'status', 'password', 'UserName']
            'where' => array(
                'LoginId' => $LoginId
            )
        );
        return $this->select($args);/* Run Select Statement*/
    }
}
