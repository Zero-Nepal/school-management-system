<?php
final class Administration extends Database
{
    public function __construct()
    {
        parent::__construct();
        $this->table('administrations');
    }

    public function getUserByID($AdministrationId)
    {
        // SELECT * FROM Administration WHERE UserName = $UserName
        // SELECT * FROM Administration WHERE remeber_token = $token
        // SELECT * FROM Administration WHERE id = $id
        // SELECT * FROM Administration
        $args = array(
            // 'fields' => 'id, name, status, password, UserName'
            // 'fields' => ['id', 'name', 'status', 'password', 'UserName']
            'where' => array(
                'AdministrationId' => $AdministrationId
            )
        );
        return $this->select($args);/* Run Select Statement*/
    }

    public function getUserByLoginID($LoginId)
    {
        // SELECT * FROM Administration WHERE UserName = $UserName
        // SELECT * FROM Administration WHERE remeber_token = $token
        // SELECT * FROM Administration WHERE id = $id
        // SELECT * FROM Administration
        $args = array(
            // 'fields' => 'id, name, status, password, UserName'
            // 'fields' => ['id', 'name', 'status', 'password', 'UserName']
            'where' => array(
                'LoginId' => $LoginId
            )
        );
        return $this->select($args);/* Run Select Statement*/
    }

    public function updateUser($data, $user_id)
    {
        $args = array(
            'where' => array(
                'LoginId' => $user_id
            )
        );
        $success = $this->update($data, $args);/* Run update Statement*/
        if ($success) {
            return $user_id;
        } else {
            return false;
        }
    }

    public function updateUserByAdministrations($data, $user_id)
    {
        $args = array(
            'where' => array(
                'AdministrationsId' => $user_id
            )
        );
        $success = $this->update($data, $args);/* Run update Statement*/
        if ($success) {
            return $user_id;
        } else {
            return false;
        }
    }
}
