<?php
require $_SERVER['DOCUMENT_ROOT'] . '/g-config/init.php';
$incharge = new Administration();
$login = new Login();
$logininf = ($login->getUserByLoginID($_SESSION['LoginId']));

if (isset($_POST, $_POST['FormType']) && !empty($_POST) && $_POST['FormType'] == 'Update') {
    if (isset($_POST) && !empty($_POST)) {
        $userInfo = $incharge->getUserByLoginID($_SESSION['LoginId']);
        if ($userInfo) {
            /* $data = array(
                'FullName' => $_POST['FullName'],
                'Email' => $_POST['Email'],
                'DateOfBirth' => $_POST['DateOfBirth'],
                'Qualification' => $_POST['Qualification'],
                'MaritalStatus' => $_POST['MaritalStatus'],
                'Contact' => $_POST['Contact'],
                'PermanentAddress' => $_POST['PermanentAddress'],
                'TemporaryAddress' => $_POST['TemporaryAddress'],
                'Gender' => $_POST['Gender']
            );
            debug($data);*/
            $data = (array_slice($_POST, 2));
            /* debug($data, true);*/
            $success = $incharge->updateUserByAdministrations($data, $_POST['Administrations']);
            if ($success) {
                redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'User details has been updated successfully.');
            } else {
                redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
            }
        } else {
            redirect(ADMIN_RENDER . '/redirect/redirect.php');
        }
    } else {
        redirect(ADMIN_RENDER . '/redirect/redirect.php');
    }
} else if (isset($_POST, $_POST['FormType']) && !empty($_POST) && $_POST['FormType'] == 'Credentials') {
    $curentPswdUs = sha1($logininf[0]->UserName . $_POST['CurrentPassword']);
    $currentPswBs = sha1($logininf[0]->BlackSalt . $_POST['CurrentPassword']);
    $uName = (str_word_count($logininf[0]->UserName, 1));
    echo 'Condition : ';
    if (!empty($_POST['UserName']) && !empty($_POST['CurrentPassword']) && !empty($_POST['NewPassword']) && !empty($_POST['RetypePassword'])  && !empty($_FILES['image']['name']) && empty($_POST['del_image'])) {
        /* Update all change salt when user update */
        echo 'Update all ';

        $user=$logininf[0]->UserType . '@' . $_POST['UserName'];
        $blkSalt=$user;
        $NwPswd = sha1($blkSalt.$_POST['NewPassword']);
        $file_name = uploadSingleImage($_FILES['image'], 'users', $logininf[0]->UserName);
        /* Condition for imag update */
        if ($file_name) {
            $data['image'] = $file_name;
            if (isset($_POST['old_image']) && !empty($_POST['old_image'])) {
                if (file_exists(UPLOAD_DIR . '/users/' . $_POST['old_image'])) {
                    unlink(UPLOAD_DIR . '/users/' . $_POST['old_image']);
                }
            }
        }

        $dataallupdate= array(
            'Image' => $file_name,
            'UserName' => $user,
            'BlackSalt'=>$blkSalt,
            'Passwd' => $NwPswd,

        );

        $successAll = $login->updateUser($dataallupdate, $logininf[0]->LoginId);

        if ($successAll) {
            redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'User updated successfully.');
        } else {
            redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
        }
        
    } /* All Update complete */ else if (!empty($_POST['UserName']) && !empty($_POST['CurrentPassword']) && empty($_POST['NewPassword']) && empty($_POST['RetypePassword'])  && !empty($_FILES['image']['name']) || !empty($_POST['del_image'])) {
        /* Update image condition  */
        if (empty($logininf[0]->Image) && empty($_POST['del_image']) && ($_FILES['image']["error"] == 0) && empty($_POST['old_image'])) {
            /* Insert */
            echo 'Insert image ';

            $file_name = uploadSingleImage($_FILES['image'], 'users', $logininf[0]->UserName);
            if ($file_name) {
                $dataimg = array(
                    'Image' => $file_name
                );
            }
            $successimg = $login->updateUser($dataimg, $logininf[0]->LoginId);
            if ($successimg) {
                redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'Photo Inserted successfully.');
            } else {
                redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
            }
        } else if ($_POST['del_image'] == $logininf[0]->Image && !empty($_POST['del_image']) && ($_FILES['image']['error'] > 0)) {
            /* Delete */
            echo 'Delete image ';
            if (isset($_POST['del_image']) && !empty($_POST['del_image'])) {
                if (file_exists(UPLOAD_DIR . '/users/' . $_POST['del_image'])) {
                    unlink(UPLOAD_DIR . '/users/' . $_POST['del_image']);
                    $dataimgDel = array(
                        'Image' => ""
                    );

                    $successimg = $login->updateUser($dataimgDel, $logininf[0]->LoginId);
                    if ($successimg) {
                        redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'Photo deleted successfully.');
                    } else {
                        redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
                    }
                }
            }
        } else {
            /* Update */
            if (isset($_FILES, $_FILES['image']) && ($_FILES['image']["error"] == 0) && empty($_POST['del_image'])) {
                echo 'Update image ';
                $file_name = uploadSingleImage($_FILES['image'], 'users', $logininf[0]->UserName);
                if ($file_name) {
                    $data['image'] = $file_name;
                    if (isset($_POST['old_image']) && !empty($_POST['old_image'])) {
                        if (file_exists(UPLOAD_DIR . '/users/' . $_POST['old_image'])) {
                            unlink(UPLOAD_DIR . '/users/' . $_POST['old_image']);
                        }
                    }
                }
                $dataimg = array(
                    'Image' => $file_name
                );
                $successimg = $login->updateUser($dataimg, $logininf[0]->LoginId);
                if ($successimg) {
                    redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'Photo updated successfully.');
                } else {
                    redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
                }
            }
        }
    } /* Image Update complete */ else if (!empty($_POST['UserName']) && !empty($_POST['CurrentPassword']) && empty($_POST['NewPassword']) && empty($_POST['RetypePassword'])  && empty($_FILES['image']['name']) && empty($_POST['del_image'])) {
        /* Update username */
        echo 'Update Username';
        if ($_POST['UserName'] != $uName[1]) {
            echo 'Update Username ';
            $datauser = array(
                'UserName' => $logininf[0]->UserType . '@' . $_POST['UserName']
            );
            $successuser = $login->updateUser($datauser, $logininf[0]->LoginId);
            if ($successuser) {
                redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'Username has been updated successfully.');
            } else {
                redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
            }
        } else {
            redirect(CMS_URL . '/incharge.php?body=settings', 'warning', 'Please enter dfferent username.');
        }
    } /* Username Update complete */ else if (!empty($_POST['UserName']) && !empty($_POST['CurrentPassword']) && !empty($_POST['NewPassword']) && !empty($_POST['RetypePassword'])  && empty($_FILES['image']['name']) && empty($_POST['del_image'])) {
        /* Update Password */
        echo 'Update Password';
        if ($logininf[0]->Passwd == $curentPswdUs || $logininf[0]->Passwd = $currentPswBs) {
            if ($_POST['NewPassword'] == $_POST['RetypePassword']) {
                if ($_POST['NewPassword'] != $_POST['CurrentPassword'] && $_POST['NewPassword'] == $_POST['RetypePassword']) {
                    echo 'Update Password';
                    $NwPswd = sha1($logininf[0]->BlackSalt . $_POST['NewPassword']);
                    $dataPswd = array(
                        'Passwd' => $NwPswd
                    );
                    $successNwPswd = $login->updateUser($dataPswd, $logininf[0]->LoginId);
                    if ($successNwPswd) {
                        redirect(CMS_URL . '/incharge.php?body=settings', 'success', 'Password been updated successfully.');
                    } else {
                        redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Server Down! Try again later.');
                    }
                } else {
                    redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Please enter different password.');
                }
            } else {
                redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'New password doesn\'t match please retype new password again.');
            }
        } else {
            redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Password Not matched plese retype password again.');
        }
    } /* Password Update complete */ else {
        redirect(CMS_URL . '/incharge.php?body=settings', 'error', 'Please enter current password to perform any task.');
    }
} else {
    redirect(ADMIN_RENDER . '/redirect/redirect.php');
}
