<?php
$login = new Login();
  if(!isset($_SESSION, $_SESSION['Token']) || empty($_SESSION) || empty($_SESSION['Token'])){/* Enters if session is not set*/
        if(isset($_COOKIE, $_COOKIE['_ua']) && !empty($_COOKIE['_ua'])){/* Enters if cookie is set*/
            $Token  = $_COOKIE['_ua'];
            $user_info = $login->getUserByToken($Token);/* Sends the token value to get userId if token is set*/
            if(!$user_info){/* Enters user_info is empty to delete cookie*/
                setcookie('_ua','',time()-60,'/');
                redirect(CMS_URL.'/','error', 'Please login first');
            }
            $_SESSION['LoginId'] = $user_info[0]->LoginId;/* Access user id using user_info for updates in session and last ip*/
            $_SESSION['UserName'] = $user_info[0]->UserName;/* Username to update value */
            $Token = getRandomString(100);
            $_SESSION['Token'] = $Token;
            $_SESSION['UserType']= $user_info[0]->UserType;
            setcookie('_ua',$Token, time()+8640000, '/');
            $data = array(
                'LastIp' => $_SERVER['REMOTE_ADDR'],
                'LastLogin' => date('Y-m-d'),
                'Status'    => 'active',
                'BlackSalt' =>$user_info[0]->PasswdHash
            );
            $data['Token'] = $Token;
            $login->updateUser($data, $user_info[0]->LoginId);
        }
        else
        {
            redirect(CMS_URL.'/','error','Please login first');
        }    
    }
