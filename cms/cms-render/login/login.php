<?php
require $_SERVER['DOCUMENT_ROOT'] . '/g-config/init.php';
$login = new Login();

if (isset($_POST) && !empty($_POST)) {
    $typ = $_POST['UserType'];
    $usr_name = $typ . '@' . $_POST['UserName'];
    $users_info = $login->getUserByUserName($usr_name);
    $blackSalter = $users_info[0]->BlackSalt;
    $userSalter = $users_info[0]->UserName;
    $pswdDb = $users_info[0]->Passwd;
    $pswdbs = sha1($blackSalter . $_POST['Passwd']);
    $pswdus = sha1($userSalter . $_POST['Passwd']);
    if ($users_info) {
        if ($pswdDb == $pswdbs || $pswdDb == $pswdus) {
            if ($users_info[0]->UserType == $typ) {
                if ($users_info[0]->Status == 'active') {
                   
                    $_SESSION['LoginId'] = $users_info[0]->LoginId;/* Access user id using users_info for updates in session and last ip*/
                    $_SESSION['UserName'] = $users_info[0]->UserName;
                    $Token = getRandomString(100);
                    $_SESSION['Token'] = $Token;
                    $_SESSION['UserType'] = $typ;
                    $data = array(
                        'LastIp' => $_SERVER['REMOTE_ADDR'],
                        'LastLogin' => date('Y-m-d'),
                        'Status'    => 'active',
                        'BlackSalt' => $typ . '@' . $_POST['UserName']
                    );
                    if (isset($_POST['remember']) && !empty($_POST['remember'])) {
                        setcookie('_ua', $Token, time() + 8640000, '/');
                        $data['Token'] = $Token;
                    }
                    $login->updateUser($data, $users_info[0]->LoginId);/* Sends the value of data to update and logine id to update data*/
                    redirect(ADMIN_RENDER . '/redirect/redirect.php', 'success', 'Welcome to ' . $typ . ' panel.');
                } else {
                    redirect(CMS_URL . '/', 'error', 'Your account is deactived.');
                }
            } else {
                redirect(CMS_URL . '/', 'error', 'You are not allowed to access this page.');
            }
        } else {
            redirect(CMS_URL . '/', 'error', 'Password does not match.');
        }
    } else {
        redirect(CMS_URL . '/', 'error', 'User not found.');
    }
} else {
    redirect(CMS_URL . '/', 'error', 'Unauthorized access! Please Login using User Name and Password.');
}
