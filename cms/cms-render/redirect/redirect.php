<?php 
	require $_SERVER['DOCUMENT_ROOT'].'/g-config/init.php';
	require '../login/checklogin.php';
	$admin = new Administration();
	$login=new Login();
	$details = $admin->getUserByLoginID($_SESSION['LoginId']);
	$info=$login->getUserByLoginID($details[0]->LoginId);

	if((isset($_SESSION,$_SESSION['UserType']) && !empty($_SESSION['UserType'])||(isset($_COOKIE,$_COOKIE['_ua']) && !empty($_COOKIE['_ua']))) ){
		if($info[0]->UserType!=""){		
			redirect(CMS_URL.'/'.lcfirst($_SESSION['UserType']).'.php');
		}else{
			redirect(CMS_URL.'/', 'error', 'You are not authorized to access this page.');
		}	
	}else{
		redirect(CMS_URL.'/', 'error', 'You are not allowed to access this page.');
	}
