<?php
require $_SERVER['DOCUMENT_ROOT'] . '/g-config/init.php';
$user = new Administration();
$userCredential = new Login();
if (isset($_SESSION, $_SESSION['UserType']) && !empty($_SESSION['UserType'])) {
    if ($_SESSION['UserType'] == 'Incharge') {
        //check condition

    } else {
        redirect(CMS_URL . '/', 'error', 'You are not allowed to access this page.');
    }
} else {
    redirect(CMS_URL . '/', 'error', 'Please login first.');
}
$title = "Incharge || " . SITE_TITLE;
require 'cms-render/login/checklogin.php';
require 'cms-inc/header.php';
require 'cms-inc/incharge-inc/incharge-sidebar.php';
if (isset($_GET['body']) && !empty($_GET['body'])) {
    require 'cms-inc/incharge-inc/incharge-' . $_GET['body'] . '.php';
} else {
    require 'cms-inc/incharge-inc/incharge-body.php';
}
require 'cms-inc/footer.php';
