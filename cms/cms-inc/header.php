<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="School Management System">
    <meta name="author" content="ZeroIndustries">
    <meta name="keywords" content="SMS">

    <!-- Title Page-->
    <title><?php echo $title; ?></title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo ADMIN_CSS; ?>/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo ADMIN_CSS; ?>/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Font awesom -->
    <!--<script src="https://kit.fontawesome.com/98c3d96e17.js"></script>-->

    <!-- Vendor CSS-->
    <link href="<?php echo ADMIN_CSS; ?>/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo ADMIN_CSS; ?>/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo ADMIN_CSS; ?>/theme.css" rel="stylesheet" media="all">
</head>