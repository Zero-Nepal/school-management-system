            <div class="section__content section__content--p30">
                <div class="overview-wrap">
                    <h2 class="title-1">Staffs</h2>
                    <button class="au-btn au-btn-icon au-btn--blue">
                        <i class="zmdi zmdi-plus"></i>Add Staffs</button>
                </div>
                <div class="container-fluid">
                    <!-- Countings -->
                    <div class="row m-t-25">
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c1">

                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-account-o"></i>
                                        </div>
                                        <div class="text">
                                            <h2>10</h2>
                                            <span>Accountant</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c2">

                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-shopping-cart"></i>
                                        </div>
                                        <div class="text">
                                            <h2>10</h2>
                                            <span>Assistant</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c3">

                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-calendar-note"></i>
                                        </div>
                                        <div class="text">
                                            <h2>10</h2>
                                            <span>Helper</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c4">

                                <div class="overview__inner">
                                    <div class="overview-box clearfix">
                                        <div class="icon">
                                            <i class="zmdi zmdi-money"></i>
                                        </div>
                                        <div class="text">
                                            <h2>10</h2>
                                            <span>Driver</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="overview-wrap">
                            <h2 class="title-1">Staffs</h2>
                        </div> <br>
                        <div class="table-responsive table--no-card m-b-40">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th class="text-center">S.N</th>
                                        <th class="text-center">Action</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Phone</th>
                                        <th class="text-center">Work Category</th>
                                        <th class="text-center">Salary (NRS)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Accountant</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Driver</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Helper</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Driver</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Assistant</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Driver</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Driver</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">8</td>
                                        <td class="text-center"><a href="#">Details</a> / <a href="#">Fire</a></td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">9844442033</td>
                                        <td class="text-center">Driver</td>
                                        <td class="text-center">10000</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-24">
                    <div class="card">
                        <div class="card-header">
                            <h3>&nbsp Staff Details</h3>
                        </div>
                        <div class="card-body card-block">
                            <!-- Profile Card -->
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="mx-auto d-block">
                                        <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">
                                    </div>
                                </div>
                                <br>
                                <!-- Profile card ends -->
                                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label"><strong>Fullname:</strong> </label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                        <div class="col col-md-3">
                                            <label for="select" class=" form-control-label">Work Category:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <select name="select" id="select" class="form-control">
                                                <option value="0">Please select</option>
                                                <option value="1">Accountant</option>
                                                <option value="2">Assistant</option>
                                                <option value="3">Driver</option>
                                                <option value="3">Helper</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Contact:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Email:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="email" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Permanent Address:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Temporary Address:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Salary:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                        <div class="col col-md-3">
                                            <label for="text-input" class=" form-control-label">Date Enrolled:</label>
                                        </div>
                                        <div class="col-12 col-md-3">
                                            <input type="date" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-dot-circle-o"></i> Update
                                </button>
                                <button type="reset" class="btn btn-danger btn-sm">
                                    <i class="fa fa-ban"></i> Cancel
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-24">
                        <!-- TOP CAMPAIGN-->
                        <div class="top-campaign">
                            <h3 class="title-3 m-b-30"><strong>Fired List</strong></h3>
                            <div class="table-responsive">
                                <table class="table table-top-campaign">
                                    <thead>
                                        <tr>
                                            <th class="text-center">S.N</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Phone</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Work Category</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Assistant</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Helper</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Accountant</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Accountant</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Assistant</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">1.</td>
                                            <td class="text-center">Binaya Karki</td>
                                            <td class="text-center">9844442033</td>
                                            <td class="text-center">Binay7587@gmail.com</td>
                                            <td class="text-center">Driver</td>
                                            <td class="text-center"><a href="#">Unfire</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--  END TOP CAMPAIGN-->
                    </div>