<!-- Batch begins-->
<div class="card">
    <div class="card-header">
        <h2 class="title-1">Batch List</h2>
        <div class="card-body">
            <p class="text-muted m-b-15">
                <button type="button" class="btn btn-primary m-l-10 m-b-10">1999 - 2000
                </button>
                <button type="button" class="btn btn-secondary m-l-10 m-b-10">2000 - 2001
                </button>
                <button type="button" class="btn btn-success m-l-10 m-b-10">2001 - 2002
                </button>
                <button type="button" class="btn btn-danger m-l-10 m-b-10">2002 - 2003
                </button>
                <button type="button" class="btn btn-warning m-l-10 m-b-10">2003 - 2004
                </button>
                <button type="button" class="btn btn-info m-l-10 m-b-10">2004 - 2005
                </button>
                <button type="button" class="btn btn-dark m-l-10 m-b-10">2005 - 2006
                </button>
                <button type="button" class="btn btn-secondary m-l-10 m-b-10">2006 - 2007
                </button>
            </p>
            <div class="card-footer">
                <button type="reset" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i> Add
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Batch Ends-->
<!-- Table begines -->
<div class="card">
    <div class="card-header">
        <h2 class="title-1">Standards</h2>
        <div class="card-body">
            <p class="text-muted m-b-15">
                <button type="button" class="btn btn-primary m-l-10 m-b-10">Class
                    <span class="badge badge-light">I</span>
                </button>
                <button type="button" class="btn btn-secondary m-l-10 m-b-10">Class
                    <span class="badge badge-light">II</span>
                </button>
                <button type="button" class="btn btn-success m-l-10 m-b-10">Class
                    <span class="badge badge-light">III</span>
                </button>
                <button type="button" class="btn btn-danger m-l-10 m-b-10">Class
                    <span class="badge badge-light">IV</span>
                </button>
                <button type="button" class="btn btn-warning m-l-10 m-b-10">Class
                    <span class="badge badge-light">V</span>
                </button>
                <button type="button" class="btn btn-info m-l-10 m-b-10">Class
                    <span class="badge badge-light">VI</span>
                </button>
                <button type="button" class="btn btn-dark m-l-10 m-b-10">Class
                    <span class="badge badge-light">VII</span>
                </button>
                <button type="button" class="btn btn-secondary m-l-10 m-b-10">Class
                    <span class="badge badge-light">VIII</span>
                </button>
                <button type="button" class="btn btn-success m-l-10 m-b-10">Class
                    <span class="badge badge-light">IX</span>
                </button>
                <button type="button" class="btn btn-danger m-l-10 m-b-10">Class
                    <span class="badge badge-light">X</span>
                </button>
            </p>
            <div class="table-responsive table--no-card m-b-40">
                <h2 class="title-1">&nbsp Students list of class <span>I</span></h2>
                <br>
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                        <tr>
                            <th class="text-center">S.N</th>
                            <th class="text-center">Action</th>
                            <th class="text-center">Students Name</th>
                            <th class="text-center">Section</th>
                            <th class="text-center">Roll no</th>
                            <th class="text-center">Traveling</th>
                            <th class="text-center">Hostel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-center"><a href="#">View</a></td>
                            <td class="text-center">Binay Karki</td>
                            <td class="text-center">A</td>
                            <td class="text-center">1</td>
                            <td class="text-center">Yes</td>
                            <td class="text-center">Yes</td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td class="text-center"><a href="#">View</a></td>
                            <td class="text-center">Binay Karki</td>
                            <td class="text-center">A</td>
                            <td class="text-center">2</td>
                            <td class="text-center">Yes</td>
                            <td class="text-center">Yes</td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td class="text-center"><a href="#">View</a></td>
                            <td class="text-center">Binay Karki</td>
                            <td class="text-center">A</td>
                            <td class="text-center">3</td>
                            <td class="text-center">Yes</td>
                            <td class="text-center">Yes</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <button type="reset" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i> Add
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Table ends -->
<!-- Add Students form -->
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>&nbsp Add Students</h3>
        </div>
        <div class="card-body card-block">
            <!-- Profile Card -->
            <div class="col-md-12">
                <div class="card-body">
                    <div class="mx-auto d-block">
                        <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">
                    </div>
                </div>
                <br>
                <!-- Profile card ends -->
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Fullname:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="select" class=" form-control-label">Username:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Password:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Email:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="email" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Parents Name:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Temporary Address:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Permanent Address:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Date of Birth:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="date" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Add
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>
        </div>
    </div>
    <!-- Form ends -->
    <!-- Form begins -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h3>&nbspStudent's Detail</h3>
            </div>
            <div class="card-body card-block">
                <!-- Profile Card -->
                <div class="col-md-12">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">
                        </div>
                    </div>
                    <br>
                    <!-- Profile card ends -->
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Full Name :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Binay Karki</p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Username :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Binay7587</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Email :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Binay7587@gmail.com</p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Roll no :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">1</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Contact :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">9861315234</p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Permanent Address :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Dolakha</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Temporary Address :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Baneshowr</p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Gender :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Male</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Date Of Birth :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">2018/12/12</p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Transport :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Yes</p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Hostel Service:</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">Full hostel</p>
                            </div>
                        </div>
                    </form>
                </div>
                <hr>
                <h3>Fee detailes</h3>
                <br>
                <div class="table-responsive table--no-card m-b-40">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th class="text-center">S.N</th>
                                <th class="text-center">Fee Type</th>
                                <th class="text-center">Issued Date</th>
                                <th class="text-center">Due Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Paid By</th>
                                <th class="text-center">Amount (NRS)</th>
                                <th class="text-center">Due Amount (NRS)</th>
                                <th class="text-center">Fine</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Monthly</td>
                                <td class="text-center">2019/01/01</td>
                                <td class="text-center">2019/12/12</td>
                                <td class="text-center">9861315234</td>
                                <td class="text-center">Not paid</td>
                                <td class="text-center">20,000</td>
                                <td class="text-center">20,000</td>
                                <td class="text-center">2000</td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Hostel Fee</td>
                                <td class="text-center">2019/01/01</td>
                                <td class="text-center">2019/12/12</td>
                                <td class="text-center">9861315234</td>
                                <td class="text-center">Not paid</td>
                                <td class="text-center">20,000</td>
                                <td class="text-center">20,000</td>
                                <td class="text-center">2000</td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Monthly</td>
                                <td class="text-center">2019/01/01</td>
                                <td class="text-center">2019/12/12</td>
                                <td class="text-center">9861315234</td>
                                <td class="text-center">Not paid</td>
                                <td class="text-center">20,000</td>
                                <td class="text-center">20,000</td>
                                <td class="text-center">2000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <h3>Report detailes</h3>
                <br>
                <div class="table-responsive table--no-card m-b-40">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th class="text-center">S.N</th>
                                <th class="text-center">Exam type</th>
                                <th class="text-center">Result collected</th>
                                <th class="text-center">GPA</th>
                                <th class="text-center">Grade</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Monthly</td>
                                <td class="text-center">False</td>
                                <td class="text-center">3.5</td>
                                <td class="text-center">A</td>
                                <td class="text-center"><a href="#">View Details</a></td>
                            </tr>
                            <tr>
                                <td class="text-center">2</td>
                                <td class="text-center">Monthly</td>
                                <td class="text-center">True</td>
                                <td class="text-center">3.5</td>
                                <td class="text-center">A</td>
                                <td class="text-center"><a href="#">View Details</a></td>
                            </tr>
                            <tr>
                                <td class="text-center">3</td>
                                <td class="text-center">Monthly</td>
                                <td class="text-center">False</td>
                                <td class="text-center">3.5</td>
                                <td class="text-center">A</td>
                                <td class="text-center"><a href="#">View Details</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <button type="reset" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Update
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Cancel
                    </button>
                </div>
            </div>
        </div>
        <!-- Form ends -->