<!-- Table begines -->
<div class="card">
    <div class="card-header">
        <h2 class="title-1">Standards</h2>
        <div class="card-body">
            <p class="text-muted m-b-15">
                <button type="button" class="btn btn-primary m-l-10 m-b-10">Class
                    <span class="badge badge-light">I</span>
                </button>
                <button type="button" class="btn btn-secondary m-l-10 m-b-10">Class
                    <span class="badge badge-light">II</span>
                </button>
                <button type="button" class="btn btn-success m-l-10 m-b-10">Class
                    <span class="badge badge-light">III</span>
                </button>
                <button type="button" class="btn btn-danger m-l-10 m-b-10">Class
                    <span class="badge badge-light">IV</span>
                </button>
                <button type="button" class="btn btn-warning m-l-10 m-b-10">Class
                    <span class="badge badge-light">V</span>
                </button>
                <button type="button" class="btn btn-info m-l-10 m-b-10">Class
                    <span class="badge badge-light">VI</span>
                </button>
                <button type="button" class="btn btn-dark m-l-10 m-b-10">Class
                    <span class="badge badge-light">VII</span>
                </button>
                <button type="button" class="btn btn-secondary m-l-10 m-b-10">Class
                    <span class="badge badge-light">VIII</span>
                </button>
                <button type="button" class="btn btn-success m-l-10 m-b-10">Class
                    <span class="badge badge-light">IX</span>
                </button>
                <button type="button" class="btn btn-danger m-l-10 m-b-10">Class
                    <span class="badge badge-light">X</span>
                </button>
            </p>
            <div class="table-responsive table--no-card m-b-40">
                <h2 class="title-1">&nbsp Students list of class <span>I</span></h2>
                <br>
                <table class="table table-borderless table-striped table-earning">
                    <thead>
                        <tr>
                            <th class="text-center">S.N</th>
                            <th class="text-center">Action</th>
                            <th class="text-center">Students Name</th>
                            <th class="text-center">Section</th>
                            <th class="text-center">Roll no</th>
                            <th class="text-center">Traveling</th>
                            <th class="text-center">Hostel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-center"><a href="#">View</a></td>
                            <td class="text-center">Binay Karki</td>
                            <td class="text-center">A</td>
                            <td class="text-center">1</td>
                            <td class="text-center">Yes</td>
                            <td class="text-center">Yes</td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td class="text-center"><a href="#">View</a></td>
                            <td class="text-center">Binay Karki</td>
                            <td class="text-center">A</td>
                            <td class="text-center">2</td>
                            <td class="text-center">Yes</td>
                            <td class="text-center">Yes</td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td class="text-center"><a href="#">View</a></td>
                            <td class="text-center">Binay Karki</td>
                            <td class="text-center">A</td>
                            <td class="text-center">3</td>
                            <td class="text-center">Yes</td>
                            <td class="text-center">Yes</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <button type="reset" class="btn btn-success btn-sm">
                    <i class="fa fa-plus"></i> Add
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Table ends -->
<div class="table-responsive table--no-card m-b-40">
    <h2 class="title-1">&nbsp Binay's Attendance details</h2>
    <br>
    <table class="table table-borderless table-striped table-earning">
        <thead>
            <tr>
                <th class="text-center">S.N</th>
                <th class="text-center">Year</th>
                <th class="text-center">Month</th>
                <th class="text-center">Day</th>
                <th class="text-center">Status</th>
                <th class="text-center">Leave Information</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">2019</td>
                <td class="text-center">January</td>
                <td class="text-center">12</td>
                <td class="text-center">Preasent</td>
                <td class="text-center"></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">2019</td>
                <td class="text-center">February</td>
                <td class="text-center">12</td>
                <td class="text-center">Preasent</td>
                <td class="text-center"></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">2019</td>
                <td class="text-center">March</td>
                <td class="text-center">12</td>
                <td class="text-center">Absent</td>
                <td class="text-center">Headache</td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">2019</td>
                <td class="text-center">April</td>
                <td class="text-center">12</td>
                <td class="text-center">Absent</td>
                <td class="text-center">Stomach ache</td>
            </tr>

        </tbody>
    </table>
</div>