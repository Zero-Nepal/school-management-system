<?php
$uName = (str_word_count($detailsInchargeCredential[0]->UserName, 1));
?>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title mb-3">Update Profile</strong>
            </div>
            <div class="card-body">
                <div class="mx-auto d-block">
                    <?php
                    if (file_exists(UPLOAD_DIR . '/users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                        ?>
                        <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo UPLOAD_URL . '/users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>">
                    <?php
                    } else {
                        ?>
                        <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo ADMIN_IMG . '/defaultuser.png'; ?>" alt="Default user">
                    <?php
                    }
                    ?>
                    <br>
                    <h5 class="text-sm-center md-2 mb-1"><?php echo $user_info[0]->FullName; ?></h5>
                    <div class="location text-sm-center">
                        <i class="fa fa-map-marker"></i> <?php echo $user_info[0]->TemporaryAddress; ?>, Nepal</div>
                </div>
                <hr>
                <div class="card-text text-sm-center">
                    <button type="button" class="btn btn-success btn-sm"><i class="fa fa-users"></i> Update</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title mb-3">Update Credentials</strong>
            </div>
            <div class="card-body">
                <div class="mx-auto d-block">
                    <?php
                    if (file_exists(UPLOAD_DIR . '/users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                        ?>
                        <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo UPLOAD_URL . '/users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>">
                    <?php
                    } else {
                        ?>
                        <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo ADMIN_IMG . '/defaultuser.png'; ?>" alt="Default user">
                    <?php
                    }
                    ?>
                    <br>
                    <h5 class="text-sm-center md-2 mb-1"><?php echo $user_info[0]->FullName; ?></h5>
                    <div class="location text-sm-center">
                        <i class="fa fa-map-marker"></i> <?php echo $user_info[0]->TemporaryAddress; ?>, Nepal</div>
                </div>
                <hr>
                <div class="card-text text-sm-center">
                    <button type="button" class="btn btn-warning btn-sm"><i class="fa fa-key"></i> Update</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title mb-3">View Profile</strong>
            </div>
            <div class="card-body">
                <div class="mx-auto d-block">
                    <?php
                    if (file_exists(UPLOAD_DIR . '/Users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                        ?>
                        <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo UPLOAD_URL . '/Users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>">
                    <?php
                    } else {
                        ?>
                        <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo ADMIN_IMG . '/defaultuser.png'; ?>" alt="Default user">
                    <?php
                    }
                    ?>
                    <br>
                    <h5 class="text-sm-center md-2 mb-1"><?php echo $user_info[0]->FullName; ?></h5>
                    <div class="location text-sm-center">
                        <i class="fa fa-map-marker"></i> <?php echo $user_info[0]->TemporaryAddress; ?>, Nepal</div>
                </div>
                <hr>
                <div class="card-text text-sm-center">
                    <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> View</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>&nbspUpdate Profile</h3>
            </div>
            <div class="card-body card-block">
                <!-- Profile Card -->
                <div class="col-md-12">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <?php
                            if (file_exists(UPLOAD_DIR . '/users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                                ?>
                                <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo UPLOAD_URL . '/users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>">
                            <?php
                            } else {
                                ?>
                                <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo ADMIN_IMG . '/defaultuser.png'; ?>" alt="Default user">
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <br>
                    <!-- Profile card ends -->
                    <?php @$detailsIncharge = ($user->getUserByLoginID($_SESSION['LoginId'])); ?>
                    <form action="<?php echo ADMIN_RENDER; ?>/incharge/settings.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" value="Update" name="FormType">
                        <div class="row form-group">
                            <input type="hidden" name="Administrations" value="<?php echo @$detailsIncharge[0]->AdministrationsId; ?>">
                            <div class="col col-md-3">
                                <label for="FullName" class=" form-control-label">Fullname:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="text" id="FullName" name="FullName" value="<?php echo @$detailsIncharge[0]->FullName; ?>" class="form-control">
                            </div>

                            <div class="col col-md-3">
                                <label for="Email" class=" form-control-label">Email:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="Email" id="Email" name="Email" value="<?php echo @$detailsIncharge[0]->Email; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="DateOfBirth" class=" form-control-label">Date Of Birth:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="date" id="DateOfBirth" name="DateOfBirth" value="<?php echo @$detailsIncharge[0]->DateOfBirth; ?>" class="form-control">
                            </div>
                            <div class="col col-md-3">
                                <label for="Qualification" class=" form-control-label">Qualification:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="text" id="Qualification" name="Qualification" value="<?php echo @$detailsIncharge[0]->Qualification; ?>" class="form-control">
                            </div>

                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="MaritalStatus" class=" form-control-label">Marital Status:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="MaritalStatus" id="MaritalStatus" class="form-control">
                                    <option value="">Please select</option>
                                    <option value="Married" <?php echo isset($detailsIncharge, $detailsIncharge[0]->MaritalStatus) && $detailsIncharge[0]->MaritalStatus == 'Married' ? 'Selected' : ''; ?>>Married</option>
                                    <option value="Unmarried" <?php echo isset($detailsIncharge, $detailsIncharge[0]->MaritalStatus) && $detailsIncharge[0]->MaritalStatus == 'Unmarried' ? 'Selected' : ''; ?>>Unmarried</option>
                                    <option value="Engaged" <?php echo isset($detailsIncharge, $detailsIncharge[0]->MaritalStatus) && $detailsIncharge[0]->MaritalStatus == 'Engaged' ? 'Selected' : ''; ?>>Engaged</option>
                                </select>
                            </div>
                            <div class="col col-md-3">
                                <label for="Contact" class=" form-control-label">Contact:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="number" id="Contact" name="Contact" value="<?php echo @$detailsIncharge[0]->Contact; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="PermanentAddress" class=" form-control-label">Permanent Address:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="text" id="PermanentAddress" name="PermanentAddress" value="<?php echo @$detailsIncharge[0]->PermanentAddress; ?>" class="form-control">
                            </div>
                            <div class="col col-md-3">
                                <label for="TemporaryAddress" class=" form-control-label">Temporary Address:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="text" id="TemporaryAddress" name="TemporaryAddress" value="<?php echo @$detailsIncharge[0]->TemporaryAddress; ?>" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="Gender" class=" form-control-label">Gender:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <select name="Gender" id="Gender" class="form-control">
                                    <option value="">Please select</option>
                                    <option value="Male" <?php echo isset($detailsIncharge, $detailsIncharge[0]->Gender) && $detailsIncharge[0]->Gender == 'Male' ? 'Selected' : ''; ?>>Male</option>
                                    <option value="Female" <?php echo isset($detailsIncharge, $detailsIncharge[0]->Gender) && $detailsIncharge[0]->Gender == 'Female' ? 'Selected' : ''; ?>>Female</option>
                                    <option value="Others" <?php echo isset($detailsIncharge, $detailsIncharge[0]->Gender) && $detailsIncharge[0]->Gender == 'Others' ? 'Selected' : ''; ?>></option>
                                </select>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Update
                            </button>
                            &nbsp;
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Profile Upload completed successfully -->
    <?php $act = "UpdateCredentials" ?>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>&nbspCredentials</h3>
            </div>
            <div class="card-body card-block">
                <!-- Profile Card -->
                <div class="col-md-12">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <?php
                            if (file_exists(UPLOAD_DIR . '/users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                                ?>
                                <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo UPLOAD_URL . '/users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>">
                            <?php
                            } else {
                                ?>
                                <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo ADMIN_IMG . '/defaultuser.png'; ?>" alt="Default user">
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <br>
                    <!-- Profile card ends -->
                    <form action="<?php echo ADMIN_RENDER; ?>/incharge/settings.php" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" value="Credentials" name="FormType">
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="UserName" class=" form-control-label">User Name:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="text" id="UserName" name="UserName" value="<?php echo @$uName[1]; ?>" class="form-control">
                            </div>
                            <div class="col col-md-3">
                                <label for="CurrentPassword" class=" form-control-label">Current Password:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="password" id="CurrentPassword" name="CurrentPassword" class="form-control">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="NewPassword" class=" form-control-label">New Password:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="password" id="NewPassword" name="NewPassword" class="form-control">
                            </div>
                            <div class="col col-md-3">
                                <label for="RetypePassword" class=" form-control-label">Retype Password:</label>
                            </div>
                            <div class="col-12 col-md-3">
                                <input type="password" id="RetypePassword" name="RetypePassword" class="form-control">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="image" class=" form-control-label">Update Image:</label>
                            </div>
                            <div class="col col-md-4">
                                <input type="file" id="image" name="image" class="form-control-file" accept="image/*">
                                <?php
                                if ($act == 'UpdateCredentials') {
                                    if (file_exists(UPLOAD_DIR . '/users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                                        ?>
                                        <div class="col-sm-4">
                                            <img src="<?php echo UPLOAD_URL . '/users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>" class="img img-responsive img-thumbnail">
                                            <input type="checkbox" name="del_image" value="<?php echo $detailsInchargeCredential[0]->Image; ?>"> Delete
                                        </div>
                                    <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <input type="hidden" name="old_image" value="<?php echo @$detailsInchargeCredential[0]->Image; ?>">
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Update
                            </button>
                            &nbsp;
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>&nbsp;My Profile</h3>
            </div>
            <div class="card-body card-block">
                <!-- Profile Card -->
                <div class="col-md-12">
                    <div class="card-body">
                        <div class="mx-auto d-block">
                            <?php
                            if (file_exists(UPLOAD_DIR . '/users/' . $detailsInchargeCredential[0]->Image) && $detailsInchargeCredential[0]->Image != '') {
                                ?>
                                <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo UPLOAD_URL . '/users/' . $detailsInchargeCredential[0]->Image; ?>" alt="<?php echo $detailsInchargeCredential[0]->Image; ?>">
                            <?php
                            } else {
                                ?>
                                <img class="rounded-circle mx-auto d-block" width="150" src="<?php echo ADMIN_IMG . '/defaultuser.png'; ?>" alt="Default user">
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <br>
                    <!-- Profile card ends -->
                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Full Name :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->FullName; ?></p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Email :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->Email; ?></p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Contact :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->Contact; ?></p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Permanent Address :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->PermanentAddress; ?></p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Temporary Address :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->TemporaryAddress; ?></p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Gender :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->Gender; ?></p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Date Of Birth :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->DateOfBirth; ?></p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Degination :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->AdministrationType; ?></p>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Qualification :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static"><?php echo @$detailsIncharge[0]->Qualification; ?></p>
                            </div>
                            <div class="col col-md-3">
                                <label class=" form-control-label"><strong>Salary :</strong></label>
                            </div>
                            <div class="col-6 col-md">
                                <p class="form-control-static">NRS <?php echo @$detailsIncharge[0]->Salary; ?></p>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Update
                            </button>
                            &nbsp;
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Cancel
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>