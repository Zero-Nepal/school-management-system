<li>
    <a href="incharge.php?body=body">
        <i class="fas fa-home"></i>Home</a>
</li>
<li class="has-sub">
    <a class="js-arrow open" href="#">
        <i class="fas fa-users-cog"></i>Administration</a>
    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
        <li>
            <a href="incharge.php?body=departments">
                <i class="fas fa-sitemap"></i>Departments</a>
        </li>
        <li>
            <a href="incharge.php?body=teachers">
                <i class="fas fa-chalkboard-teacher"></i>Teachers</a>
        </li>
        <li>
            <a href="incharge.php?body=syllabus">
                <i class="fas fa-scroll "></i>Syllabus</a>
        </li>
    </ul>
</li>
<li class="has-sub">
    <a class="js-arrow open" href="#">
        <i class="fas fa-users"></i>Management</a>
    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
        <li>
            <a href="incharge.php?body=finance">
                <i class="fas fa-coins"></i>Finance</a>
        </li>
        <li>
            <a href="incharge.php?body=vehicles">
                <i class="fas fa-bus-alt"></i>Vehicle</a>
        </li>
        <li>
            <a href="incharge.php?body=staffs">
                <i class="fas fa-user-edit"></i>Staffs</a>
        </li>
    </ul>
</li>
<li class="has-sub">
    <a class="js-arrow open" href="#">
        <i class="fas fa-id-card-alt"></i>Students Info</a>
    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
        <li>
            <a href="incharge.php?body=students">
                <i class="fas fa-user-graduate"></i>Students</a>
        </li>
        <li>
            <a href="incharge.php?body=parents">
                <i class="fas fa-user-friends"></i>Parents</a>
        </li>
        <li>
            <a href="incharge.php?body=attendance">
                <i class="fas fa-clipboard"></i>Attendance</a>
        </li>
        <li>
            <a href="incharge.php?body=results">
                <i class="fas fa-poll"></i>Result</a>
        </li>
        <li>
            <a href="incharge.php?body=batch">
                <i class="fas fa-calendar-alt"></i>Batch</a>
        </li>
    </ul>
</li>
<li>
    <a href="incharge.php?body=admission">
        <i class="fas fa-hand-holding-usd"></i>Admission</a>
</li>
<li>
    <a href="incharge.php?body=noticeboard">
        <i class="fas fa-chalkboard"></i>Noticeboard</a>
</li>
<li>
    <a href="incharge.php?body=article">
        <i class="fas fa-newspaper"></i>Article</a>
</li>