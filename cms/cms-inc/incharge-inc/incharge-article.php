<div class="overview-wrap">
    <h2 class="title-1">Article Approval</h2>
</div> <br>
<div class="table-responsive table--no-card m-b-40">
    <table class="table table-borderless table-striped table-earning">
        <thead>
            <tr>
                <th class="text-center">S.N</th>
                <th class="text-center">Student Name</th>
                <th class="text-center">Topic</th>
                <th class="text-center">Date</th>
                <th class="text-center">Description</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Parents</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Teachers</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Parents</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Teachers</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Parents</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Teachers</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Parents</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">Teachers</td>
                <td class="text-center">Holi Holiday</td>
                <td class="text-center">2019/06/30</td>
                <td class="text-center">School remains closed</td>
                <td class="text-center"><a href="#">Approve</a> / <a href="#">Reject</a></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-lg-6">
        <!-- TOP CAMPAIGN-->
        <div class="top-campaign">
            <h3 class="title-3 m-b-30">Approved List</h3>
            <div class="table-responsive">
                <table class="table table-top-campaign">
                    <thead>
                        <tr>
                            <th class="text-center">Date</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Topic</th>
                            <th class="text-center">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--  END TOP CAMPAIGN-->
    </div>
    <div class="col-lg-6">
        <!-- TOP CAMPAIGN-->
        <div class="top-campaign">
            <h3 class="title-3 m-b-30">Rejected List</h3>
            <div class="table-responsive">
                <table class="table table-top-campaign">
                    <thead>
                        <tr>
                            <th class="text-center">Date</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Topic</th>
                            <th class="text-center">Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                        <tr>
                            <td>2019/01/01</td>
                            <td>Binaya Karki</td>
                            <td>Pollution</td>
                            <td>Pollution is pollution and is very dangerous. Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores maiores asperiores tempora pariatur, porro ipsa a cumque, consectetur error sed magnam, quod voluptates facilis libero nulla dolor beatae distinctio tenetur!</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--  END TOP CAMPAIGN-->
    </div>
</div>