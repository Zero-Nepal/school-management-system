<div class="overview-wrap">
    <h2 class="title-1">Vechiles Overview</h2>
    <button class="au-btn au-btn-icon au-btn--blue">
        <i class="zmdi zmdi-plus"></i>add vehicles</button>
</div>
<div class="section__content section__content--p30">
    <div class="container-fluid">
        <!-- Countings -->
        <div class="row m-t-25">
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c1">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-account-o"></i>
                            </div>
                            <div class="text">
                                <h2>10</h2>
                                <span>Bus count</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c2">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-shopping-cart"></i>
                            </div>
                            <div class="text">
                                <h2>3</h2>
                                <span>Van count</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c3">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-calendar-note"></i>
                            </div>
                            <div class="text">
                                <h2>1</h2>
                                <span>Micro Van</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="overview-item overview-item--c3">
                    <div class="overview__inner">
                        <div class="overview-box clearfix">
                            <div class="icon">
                                <i class="zmdi zmdi-calendar-note"></i>
                            </div>
                            <div class="text">
                                <h2>1</h2>
                                <span>Micro Van</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive table--no-card m-b-40">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th class="text-center">S.N</th>
                                <th class="text-center">Vehicle number</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Vehicle route</th>
                                <th class="text-center">Students count</th>
                                <th class="text-center">Drivers Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">978989</td>
                                <td class="text-center"><a href="#">View Driver</a></td>
                                <td class="text-center">Baneshowr - Chabihel</td>
                                <td class="text-center">30</td>
                                <td class="text-center">Binay Karki</td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">978989</td>
                                <td class="text-center"><a href="#">View Driver</a></td>
                                <td class="text-center">Baneshowr - Chabihel</td>
                                <td class="text-center">30</td>
                                <td class="text-center">Binay Karki</td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">978989</td>
                                <td class="text-center"><a href="#">View Driver</a></td>
                                <td class="text-center">Baneshowr - Chabihel</td>
                                <td class="text-center">30</td>
                                <td class="text-center">Binay Karki</td>
                            </tr>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">978989</td>
                                <td class="text-center"><a href="#">View Driver</a></td>
                                <td class="text-center">Baneshowr - Chabihel</td>
                                <td class="text-center">30</td>
                                <td class="text-center">Binay Karki</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Table Details Ends-->
        <!-- Form begins -->
        <div class="col-lg-24">
            <div class="card">
                <div class="card-header">
                    <h3>&nbspDriver Details</h3>
                </div>
                <div class="card-body card-block">
                    <!-- Profile Card -->
                    <div class="col-md-12">
                        <div class="card-body">
                            <div class="mx-auto d-block">
                                <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">
                            </div>
                        </div>
                        <br>
                        <!-- Profile card ends -->
                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Full Name :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Binay Karki</p>
                                </div>
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Username :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Binay7587</p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Email :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Binay7587@gmail.com</p>
                                </div>
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Date Of Birth :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">2018/12/12</p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-6 col-md-3">
                                    <label class=" form-control-label"><strong>Marital Status :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Unmarried</p>
                                </div>
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Salary :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">20000</p>
                                </div>
                            </div>
                            <div class="row form-group">
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Contact :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">9861315234</p>
                                </div>
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Permanent Address :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Dolakha</p>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Temporary Address :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Baneshowr</p>
                                </div>
                                <div class="col col-md-3">
                                    <label class=" form-control-label"><strong>Gender :</strong></label>
                                </div>
                                <div class="col-6 col-md">
                                    <p class="form-control-static">Male</p>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Cancel
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form ends -->