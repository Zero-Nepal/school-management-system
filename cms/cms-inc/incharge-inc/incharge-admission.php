<div class="col-lg-24">
    <div class="card">
        <div class="card-header">
            <h3>&nbsp Admission</h3>
        </div>
        <div class="card-body card-block">
            <div class="col-md-12">
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">First Name: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Last Name: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">DOB: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Nationality: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Contact:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Email:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="email" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Permanent Address:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Temporary Address:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="select" class=" form-control-label">Sex</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <select name="select" id="select" class="form-control">
                                <option value="0">Please select</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                                <option value="3">Others</option>
                            </select>
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Class:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Section:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Date Enrolled:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="date" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <div class="card-header">
                <h5>&nbsp Parental Information</h5>
            </div>
            <div class="card-body card-block">
                <div class="col-md-12">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Father's Name: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Occupation: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Address: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Phone: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Mother's Name: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Occupation: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Address: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Phone: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Local Guardian: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Relation: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Address: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Phone: </label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Add
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>

        </div>
    </div>