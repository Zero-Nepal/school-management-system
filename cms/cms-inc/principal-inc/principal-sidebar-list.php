    <li>
        <a href="principal.php?body=body">
            <i class="fas fa-home"></i>Home</a>
    </li>

    <li class="has-sub">
        <a class="js-arrow open" href="#">
            <i class="fas fa-users-cog"></i>Administration</a>
        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
            <li>
                <a href="principal.php?body=teachers">
                    <i class="fas fa-chalkboard-teacher"></i>Teachers</a>
            </li>
            <li>
                <a href="principal.php?body=departments">
                    <i class="fas fa-sitemap"></i>Departments</a>
            </li>
        </ul>
    </li>

    <li class="has-sub">
        <a class="js-arrow open" href="#">
            <i class="fas fa-users"></i>Management</a>
        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
            <li>
                <a href="principal.php?body=finance">
                    <i class="fas fa-coins"></i>Finance</a>
            </li>
            <li>
                <a href="principal.php?body=vehicles">
                    <i class="fas fa-bus-alt"></i>Vehicles</a>
            </li>
            <li>
                <a href="principal.php?body=staffs">
                    <i class="fas fa-user-edit"></i>Staffs</a>
            </li>
        </ul>
    </li>

    <li class="has-sub">
        <a class="js-arrow open" href="#">
            <i class="fas fa-id-card-alt"></i>Students info</a>
        <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
            <li>
                <a href="principal.php?body=parents">
                    <i class="fas fa-user-friends"></i>Parents</a>
            </li>
            <li>
                <a href="principal.php?body=students">
                    <i class="fas fa-user-graduate"></i>Students</a>
            </li>
            <li>
                <a href="principal.php?body=results">
                    <i class="fas fa-poll"></i>Results</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="principal.php?body=noticeboard">
            <i class="fas fa-chalkboard"></i>Noticeboard</a>
    </li>