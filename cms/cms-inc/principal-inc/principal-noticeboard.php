<div class="row">
    <div class="col-lg-12">
        <div class="overview-wrap">
            <h2 class="title-1">Noticeboard</h2>
            <button class="au-btn au-btn-icon au-btn--blue">
                <i class="zmdi zmdi-plus"></i>Add Notice</button>
        </div> <br>
        <div class="table-responsive table--no-card m-b-40">
            <table class="table table-borderless table-striped table-earning">
                <thead>
                    <tr>
                        <th class="text-center">S.N</th>
                        <th class="text-center">To</th>
                        <th class="text-center">Topic</th>
                        <th class="text-center">Date</th>
                        <th class="text-center">Description</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Parents</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Teachers</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Parents</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Teachers</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Parents</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Teachers</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Parents</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Teachers</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Parents</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Teachers</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Parents</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Teachers</td>
                        <td class="text-center">Holi Holiday</td>
                        <td class="text-center">2019/06/30</td>
                        <td class="text-center">School remains closed</td>
                        <td class="text-center"><a href="#">View or Edit</a> / <a href="#">Delete</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-lg-24">
    <div class="card">
        <div class="card-header">
            <h3>&nbsp Add Notice</h3>
        </div>
        <div class="card-body card-block">
            <!-- Profile Card -->
            <div class="col-md-12">
                <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">To:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Topic:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Date:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <input type="date" id="text-input" name="text-input" placeholder="Text" class="form-control">
                        </div>
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Description:</label>
                        </div>
                        <div class="col-12 col-md-3">
                            <textarea name="text-input" id="text-input" cols="5" rows="5" placeholder="Text" class="form-control"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Add
                </button>
                <button type="reset" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Cancel
                </button>
            </div>
        </div>
    </div>