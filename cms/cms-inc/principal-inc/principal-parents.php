<div class="card">
    <div class="card-header">
            <h3>&nbspStandard</h3>
    </div>
            <div class="card-body">
                 <p class="text-muted m-b-15">
                    <button type="button" class="btn btn-primary m-l-10 m-b-10">Class
                      <span class="badge badge-light">I</span>
                    </button>
                    <button type="button" class="btn btn-secondary m-l-10 m-b-10">Class
                      <span class="badge badge-light">II</span>
                    </button>
                    <button type="button" class="btn btn-success m-l-10 m-b-10">Class
                      <span class="badge badge-light">III</span>
                    </button>
                    <button type="button" class="btn btn-danger m-l-10 m-b-10">Class
                      <span class="badge badge-light">IV</span>
                    </button>
                    <button type="button" class="btn btn-warning m-l-10 m-b-10">Class
                      <span class="badge badge-light">V</span>
                    </button>
                    <button type="button" class="btn btn-info m-l-10 m-b-10">Class
                      <span class="badge badge-light">VI</span>
                    </button>
                    <button type="button" class="btn btn-dark m-l-10 m-b-10">Class
                      <span class="badge badge-light">VII</span>
                    </button>
                    <button type="button" class="btn btn-secondary m-l-10 m-b-10">Class
                      <span class="badge badge-light">VIII</span>
                    </button>
                    <button type="button" class="btn btn-success m-l-10 m-b-10">Class
                      <span class="badge badge-light">IX</span>
                    </button>
                    <button type="button" class="btn btn-danger m-l-10 m-b-10">Class
                      <span class="badge badge-light">X</span>
                    </button>
                 </p>
                 <div class="table-responsive table--no-card m-b-40">
                        <table class="table table-borderless table-striped table-earning">
                            <thead>
                                <tr>
                                    <th class="text-center">S.N</th>                                                                        
                                    <th class="text-center">Action</th>
                                    <th class="text-center">Parents Name</th>
                                    <th class="text-center">Childrens Name</th>
                                    <th class="text-center">Username</th>
                                    <th class="text-center">Section</th>
                                    <th class="text-center">Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-center"><a href="#">View Details</a></td>
                                    <td class="text-center">Gita Karki</td>
                                    <td class="text-center">Binaya Karki</td>
                                    <td class="text-center">Gita7587</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">9844096501</td>                                    
                                </tr>
                            </tbody>
                        </table>    
                    </div>
                    <div class="card-footer">
                            <button type="reset" class="btn btn-success btn-sm">
                                <i class="fa fa-plus"></i> Add
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Cancel
                            </button>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3>&nbsp Add Parents</h3>
                                    </div>
                                    <div class="card-body card-block">
                                        <!-- Profile Card -->
                                        <div class="col-md-12">
                                        <div class="card-body">
                                                <div class="mx-auto d-block">
                                                    <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">                                                     
                                                </div>
                                        </div>
                                        <br>
                                        <!-- Profile card ends -->
                                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Parents Name: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Children Name: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>                   
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Username: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Password:</label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>  
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Section: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Contact: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Temporary Address: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Permanent Address: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>          
            </form>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Add
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Cancel
                                        </button>
                                    </div>
                                </div>
                    </div>
<!-- Table Details Ends-->
<!-- Form begins -->
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3>&nbspParent's Detail</h3>
        </div>
        <div class="card-body card-block">
        <!-- Profile Card -->
        <div class="col-md-12">
            <div class="card-body">
                <div class="mx-auto d-block">
                    <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">                                                     
                </div>
            </div>
            <br>
            <!-- Profile card ends -->
            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Parents Name: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Children Name: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>                   
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Username: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Section: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>  
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Gender: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Contact: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Temporary Address: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                    <div class="col col-md-3">
                        <label for="text-input" class=" form-control-label">Permanent Address: </label>
                    </div>
                    <div class="col-12 col-md-3">
                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">                                                    
                    </div>
                </div>          
            </form>
        </div>
        <hr>
        <h3>Fee Details</h3>
        <br>
        <div class="table-responsive table--no-card m-b-40">
            <table class="table table-borderless table-striped table-earning">
                <thead>
                    <tr>
                        <th class="text-center">S.N</th>
                        <th class="text-center">Fee Type</th>
                        <th class="text-center">Issued Date</th>
                        <th class="text-center">Due Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Paid By</th>
                        <th class="text-center">Amount (NRS)</th>
                        <th class="text-center">Due Amount (NRS)</th>
                        <th class="text-center">Fine</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Monthly</td>
                        <td class="text-center">2019/01/01</td>
                        <td class="text-center">2019/12/12</td>
                        <td class="text-center">9861315234</td>
                        <td class="text-center">Not paid</td>
                        <td class="text-center">20,000</td>
                        <td class="text-center">20,000</td>
                        <td class="text-center">2000</td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Hostel Fee</td>
                        <td class="text-center">2019/01/01</td>
                        <td class="text-center">2019/12/12</td>
                        <td class="text-center">9861315234</td>
                        <td class="text-center">Not paid</td>
                        <td class="text-center">20,000</td>
                        <td class="text-center">20,000</td>
                        <td class="text-center">2000</td>
                    </tr>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Monthly</td>
                        <td class="text-center">2019/01/01</td>
                        <td class="text-center">2019/12/12</td>
                        <td class="text-center">9861315234</td>
                        <td class="text-center">Not paid</td>
                        <td class="text-center">20,000</td>
                        <td class="text-center">20,000</td>
                        <td class="text-center">2000</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer">
             <button type="submit" class="btn btn-primary btn-sm">
                 <i class="fa fa-dot-circle-o"></i> Update
            </button>
            <button type="reset" class="btn btn-danger btn-sm">
                <i class="fa fa-ban"></i> Cancel
            </button>
        </div>
    </div>
</div>
<!-- Form ends -->

