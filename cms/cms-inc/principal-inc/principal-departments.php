            <div class="section__content section__content--p30">
                <div class="overview-wrap">
                    <h2 class="title-1">Departments</h2>
                    <button class="au-btn au-btn-icon au-btn--blue">
                        <i class="zmdi zmdi-plus"></i>Add Departments</button>
                </div>
                <div class="container-fluid">
                    <!-- Countings -->
                    <div class="row m-t-25">
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c1">
                                <a href="#">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-account-o"></i>
                                            </div>
                                            <div class="text">
                                                <h2>Science</h2>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c2">
                                <a href="#">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-shopping-cart"></i>
                                            </div>
                                            <div class="text">
                                                <h2>Maths &nbsp </h2>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c3">
                                <a href="#">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-calendar-note"></i>
                                            </div>
                                            <div class="text">
                                                <h2>Sociology</h2>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="overview-item overview-item--c4">
                                <a href="#">
                                    <div class="overview__inner">
                                        <div class="overview-box clearfix">
                                            <div class="icon">
                                                <i class="zmdi zmdi-money"></i>
                                            </div>
                                            <div class="text">
                                                <h2>English &nbsp</h2>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-24">
                    <div class="card">
                        <div class="card-header">
                            <h3>Add Departments</h3>
                        </div>
                        <div class="card-body card-block">
                            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="row form-group">
                                    <div class="col col-md-3">
                                        <label for="text-input" class=" form-control-label"><strong>Name:</strong> </label>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                    </div>
                                    <div class="col col-md-3">
                                        <label for="text-input" class=" form-control-label"><strong>Subject:</strong> </label>
                                    </div>
                                    <div class="col-12 col-md-3">
                                        <input type="text" id="text-input" name="text-input" placeholder="Text" class="form-control">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm">
                                <i class="fa fa-dot-circle-o"></i> Add
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm">
                                <i class="fa fa-ban"></i> Cancel
                            </button>
                        </div>
                    </div>
                </div>
                <div class="overview-wrap">
                    <h2 class="title-1">Department Details</h2>
                </div> <br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive table--no-card m-b-40">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th class="text-center">S.N</th>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Subject</th>
                                        <th class="text-center">Qualification</th>
                                        <th class="text-center">Level</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">1</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Bachelor</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">2</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">3</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">4</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">5</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">6</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">7</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">8</td>
                                        <td class="text-center">Binaya Karki</td>
                                        <td class="text-center">Mathss</td>
                                        <td class="text-center">Master</td>
                                        <td class="text-center">Secondary</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>