                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive table--no-card m-b-40">
                                    <table class="table table-borderless table-striped table-earning">
                                        <thead>
                                            <tr>
                                                <th class="text-center">S.N</th>
                                                <th class="text-center">Action</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Phone</th>
                                                <th class="text-center">Department</th>
                                                <th class="text-center">Salary (NRS)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">1</td>
                                                <td class="text-center"><a href="#">Details</a>/<a href="#">Fire</a></td>
                                                <td class="text-center">Binay Karki</td>
                                                <td class="text-center">9861315234</td>
                                                <td class="text-center">Science</td>
                                                <td class="text-center">200</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">2</td>
                                                <td class="text-center"><a href="#">Details</a>/<a href="#">Fire</a></td>
                                                <td class="text-center">Binay Karki</td>
                                                <td class="text-center">9861315234</td>
                                                <td class="text-center">Science</td>
                                                <td class="text-center">200</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">3</td>
                                                <td class="text-center"><a href="#">Details</a>/<a href="#">Fire</a></td>
                                                <td class="text-center">Binay Karki</td>
                                                <td class="text-center">9861315234</td>
                                                <td class="text-center">Science</td>
                                                <td class="text-center">200</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">4</td>
                                                <td class="text-center"><a href="#">Details</a>/<a href="#">Fire</a></td>
                                                <td class="text-center">Binay Karki</td>
                                                <td class="text-center">9861315234</td>
                                                <td class="text-center">Science</td>
                                                <td class="text-center">200</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Table Details Ends-->
                        <!-- Form begins -->
                        <div class="col-lg-24">
                            <div class="card">
                                <div class="card-header">
                                    <h3>&nbspTeachers Details</h3>
                                </div>
                                <div class="card-body card-block">
                                    <!-- Profile Card -->
                                    <div class="col-md-12">
                                        <div class="card-body">
                                            <div class="mx-auto d-block">
                                                <img class="rounded-circle mx-auto d-block" src="cms-assets/img/icon/avatar-01.jpg" alt="Card image cap">
                                            </div>
                                        </div>
                                        <br>
                                        <!-- Profile card ends -->
                                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group">

                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Full Name :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Binay Karki</p>
                                                </div>
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Username :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Binay7587</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Email :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Binay7587@gmail.com</p>
                                                </div>
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Date Of Birth :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">2018/12/12</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Subject :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Nepali</p>
                                                </div>
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Department :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Nepali</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Qualification :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Btech-Hnd in computing</p>
                                                </div>
                                                <div class="col-6 col-md-3">
                                                    <label class=" form-control-label"><strong>Marital Status :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Unmarried</p>
                                                </div>
                                            </div>
                                            <div class="row form-group">

                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Contact :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">9861315234</p>
                                                </div>
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Permanent Address :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Dolakha</p>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Temporary Address :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Baneshowr</p>
                                                </div>
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Gender :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Male</p>
                                                </div>
                                            </div>


                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Salary :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">20000</p>
                                                </div>
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"><strong>Transport :</strong></label>
                                                </div>
                                                <div class="col-6 col-md">
                                                    <p class="form-control-static">Yes</p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer">
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <!-- Form ends -->

                            <div class="col-lg-24">
                                <!-- Fired List-->
                                <div class="top-campaign">
                                    <h3 class="title-3 m-b-30"><strong>Fired List</strong></h3>
                                    <div class="table-responsive">
                                        <table class="table table-top-campaign">
                                            <thead>
                                                <th class="text-center">S.N</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center">Phone</th>
                                                <th class="text-center">Email</th>
                                                <th class="text-center">Subject</th>
                                                <th class="text-center">Action</th>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="text-center">1</td>
                                                    <td class="text-center">Binay Karki</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center">BinayKarki@hotmail.com</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center"><a href="#">Unfire</a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">3</td>
                                                    <td class="text-center">Binay Karki</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center">BinayKarki@hotmail.com</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center"><a href="#">Unfire</a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">3</td>
                                                    <td class="text-center">Binay Karki</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center">BinayKarki@hotmail.com</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center"><a href="#">Unfire</a></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center">4</td>
                                                    <td class="text-center">Binay Karki</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center">BinayKarki@hotmail.com</td>
                                                    <td class="text-center">9861315234</td>
                                                    <td class="text-center"><a href="#">Unfire</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!--  END Fired List-->
                            </div>