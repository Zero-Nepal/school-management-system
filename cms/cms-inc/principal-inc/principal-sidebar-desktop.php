        <!-- MENU SIDEBAR DESKTOP VIEW -->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="cms-assets/img/icon/bt.png" alt="School Management" />
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <?php require 'principal-sidebar-list.php'; ?>
                        <!-- Requires all the sidebar list -->
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR DESKTOP VIEW -->