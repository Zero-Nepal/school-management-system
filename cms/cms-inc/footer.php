 <!-- FOOTER -->
 <section>
     <div class="container-fluid">
         <div class="row">
             <div class="col-md-12">
                 <div class="copyright">
                     <p>Copyright © 2019 Zero. All rights reserved.</p>
                 </div>
             </div>
         </div>
     </div>
 </section>
 <!-- END FOOTER -->
 </div>
 <!-- Jquery JS -->
 <script src="<?php echo ADMIN_JS; ?>/jquery-3.2.1.min.js"></script>
 <!-- Bootstrap JS -->
 <script src="<?php echo ADMIN_JS; ?>/popper.min.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/bootstrap.min.js"></script>
 <!-- Vendor JS -->
 <script src="<?php echo ADMIN_JS; ?>/slick.min.js">
 </script>
 <script src="<?php echo ADMIN_JS; ?>/wow.min.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/animsition.min.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/bootstrap-progressbar.min.js">
 </script>
 <script src="<?php echo ADMIN_JS; ?>/jquery.waypoints.min.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/jquery.counterup.min.js">
 </script>
 <script src="<?php echo ADMIN_JS; ?>/circle-progress.min.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/perfect-scrollbar.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/Chart.bundle.min.js"></script>
 <script src="<?php echo ADMIN_JS; ?>/select2.min.js">
 </script>
 <!-- Main JS -->
 <script src="<?php echo ADMIN_JS; ?>/main.js"></script>
 <!-- Change JS -->
 <script>
     setTimeout(function() {
         $('.alert').slideUp();
     }, 4000);
 </script>
 </body>

 </html>
 <!-- end document-->