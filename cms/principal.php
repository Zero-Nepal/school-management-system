<?php 
       require $_SERVER['DOCUMENT_ROOT'].'/g-config/init.php';

       if(isset($_SESSION,$_SESSION['UserType']) && !empty($_SESSION['UserType'])){
           if($_SESSION['UserType']=='Principal'){

           }else{
            redirect(CMS_URL.'/','error','You are not allowed to access this page.');
           }
       }else{
        redirect(CMS_URL.'/','error','Please login first.');
       }

       $title="Principal || ".SITE_TITLE;
       require 'cms-render/login/checklogin.php';

        require 'cms-inc/header.php';
        require 'cms-inc/principal-inc/principal-sidebar.php';
        // echo '<pre>';
        //  print_r($_SESSION);
        //  echo'<br>';
        //  print_r($_COOKIE); 
        if(isset($_GET['body'])&&!empty($_GET['body'])){
            require 'cms-inc/principal-inc/principal-'.$_GET['body'].'.php'; 
        }
        else{
            require 'cms-inc/principal-inc/principal-body.php'; 
        } 
        require 'cms-inc/footer.php';
